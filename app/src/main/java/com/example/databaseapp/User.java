package com.example.databaseapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey
    private int uid;
    private int age;
    private int rating;
    // getters and setters are ignored for brevity but they are required for Room to work.

    public User(){}
    public int getUid(){return this.uid;}
    public int getAge(){ return this.age; }
    public int getRating(){return this.rating; }

    public void setUid(int uid){ this.uid = uid;}
    public void setAge(int age){ this.age = age;}
    public void setRating(int rating){this.rating = rating;}
}