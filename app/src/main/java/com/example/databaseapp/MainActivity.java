package com.example.databaseapp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase db = Room
            .databaseBuilder(getApplicationContext(), AppDatabase.class, "database-name")
            .allowMainThreadQueries() // SHOULD NOT BE USED IN PRODUCTION !!!
                 .fallbackToDestructiveMigration()
            .build();

        User u = new User();
        u.setUid(1113);
        u.setAge(25);
        u.setRating(8);
       // db.userDao().insertAll(u);
        List<User> userList = db.userDao().loadAll();
        Log.d("FirstUser",userList.get(0).getAge() + " " + userList.get(0).getRating());

        Button btnBarChart = findViewById(R.id.btnBarChart);

        btnBarChart.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(MainActivity.this, BarChartActivity.class);
                startActivity(i);
            }
        });

        Button btnPieChart = findViewById(R.id.btnPieChart);

        btnPieChart.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(MainActivity.this, PieChartActivity.class);
                startActivity(i);
            }
        });
    }
}
