package com.example.databaseapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class PieChartActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_piechart);

        PieChart pieChart = findViewById(R.id.piechart);
        ArrayList aantalStudenten = new ArrayList();

        aantalStudenten.add(new Entry(6,0));//18
        aantalStudenten.add(new Entry(5,1));
        aantalStudenten.add(new Entry(5,2));
        aantalStudenten.add(new Entry(5,3));

        PieDataSet leeftijd = new PieDataSet(aantalStudenten,"Aantal x");

        ArrayList<String> aantalJaarOud = new ArrayList();

        aantalJaarOud.add("18");
        aantalJaarOud.add("19");
        aantalJaarOud.add("20");
        aantalJaarOud.add("21");
        PieData data = new PieData(aantalJaarOud,leeftijd);
        pieChart.setData(data);
        leeftijd.setColors(ColorTemplate.VORDIPLOM_COLORS);
        pieChart.animateXY(5000,5000);


    }
}
