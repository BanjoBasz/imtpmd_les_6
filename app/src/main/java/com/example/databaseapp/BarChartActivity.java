package com.example.databaseapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class BarChartActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barchart);

        //Eerste wat je doet is de chart zelf vinden in de layout
        BarChart barChart = findViewById(R.id.barchart);

        ArrayList<BarEntry> aantalFeestdagenGevierd = new ArrayList();
        aantalFeestdagenGevierd.add(new BarEntry(5,0));//2015
        aantalFeestdagenGevierd.add(new BarEntry(7,1));//2016
        aantalFeestdagenGevierd.add(new BarEntry(3,2));
        aantalFeestdagenGevierd.add(new BarEntry(9,3));
        aantalFeestdagenGevierd.add(new BarEntry(1,4));

        ArrayList<String> years = new ArrayList<>();

        years.add("2015");
        years.add("2016");
        years.add("2017");
        years.add("2018");
        years.add("2019");

        BarDataSet barDataSet = new BarDataSet(aantalFeestdagenGevierd,"Aantal feestdag gevierd");
        BarData data = new BarData(years,barDataSet);

        barChart.animateY(3000);
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        barChart.setData(data);





    }
}
